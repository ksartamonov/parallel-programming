#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "mpi.h"
#include "func.c"

#define ROOT 0 // ранг главного процесса

void fill_layer(int k, int m, double** u) {
    if (m != x_max / x_step - 1) {
        // центральная явная трехточечная схема
        u[k + 1][m] = func(k, m) * t_step + 1/2.*(u[k][m+1] + u[k][m-1]) - t_step/2./x_step * (u[k][m+1] - u[k][m-1]);    
    } else {
        // явный левый уголок
        u[k + 1][m] = func(k, m) * t_step + u[k][m] - t_step/x_step * (u[k][m] - u[k][m-1]);      
    }

}

// Печать количества процессов и времени выполнения
void output_results(int size, double t1) {
    double t2 = MPI_Wtime();
    printf("%d %lg\n", size, t2 - t1);
    FILE *fp;
    fp = fopen("results.csv", "a");
    fprintf(fp, "%d,%lg\n", size, t2 - t1);
    fclose(fp);
}


int main(int argc, char** argv) {
    int WRITE_OUTPUT_CSV = 0;
    if (argc == 2) { 
        if (strcmp(argv[1], "-csv") == 0) { // опция "-csv" - запись в файл "output.csv"
            WRITE_OUTPUT_CSV = 1;
        }
    }

    int M = x_max / x_step; // количество узлов по x
    int K = t_max / t_step; // количество узлов по t

	double t1, t2;   // время начала и конца выполнения соответственно
	int rank, size;    // ранг процесса и количество процессов

	MPI_Init(&argc, &argv);
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
    MPI_Comm_size( MPI_COMM_WORLD, &size );

    double **u = (double **)calloc(K, sizeof(double *));
    for (int i = 0; i < K; i++) {
        u[i] = (double *)calloc(M, sizeof(double));
    }
    
    if(rank == ROOT) {
    	t1 = MPI_Wtime();      // начало отсчета времени
    }

    for (int m = 0; m < M; ++m)
        u[0][m] = fi(m);           // инициализация при t = 0

    for (int k = 0; k < K; ++k)
        u[k][0] = ksi(k);           // инициализация при x = 0

    if (size > 1) {  // эта часть выполняется в случае параллельной программы
        int part = M / size;      // количество вычисляемых точек для всех процессов, кроме главного
        int shift = M % size;     // количество сдвига (доп точки для главного процесса)
        int num_knots = (rank == ROOT) ? ( part + shift ) : part;   // в зависимости от процесса выбор количества вычисляемых точек
        int x_0 = (rank == ROOT) ? (0) : ( part * rank + shift);    // первая точка вычислений
        int x_1 = x_0 + num_knots;         // последняя точка вычислений (не включительно)


        for (int k = 0; k < K - 1; k++){ // время
        	for (int m = x_0 + 1; m < x_1 - 1; m++) { // координата
        		fill_layer(k, m, u);             // заполняем все внутренние точки, неоходимые данные для вычислений которых процесс уже знает
        	}
        	if (rank != ROOT && rank != size - 1) {
        		if (k > 0){
        			MPI_Recv(&u[k][x_0 - 1], 1, MPI_DOUBLE, rank - 1, k, MPI_COMM_WORLD, MPI_STATUS_IGNORE);   // принимаем соседние точки к крайним
    		    	MPI_Recv(&u[k][x_1], 1, MPI_DOUBLE, rank + 1, k, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        		}
    	    	fill_layer(k, x_0, u);       // вычисляем для соседних точек
    	    	fill_layer(k, x_1 - 1, u);
    	    	if (k < K - 2){
    	    		MPI_Send(&u[k + 1][x_0], 1, MPI_DOUBLE, rank - 1, k + 1, MPI_COMM_WORLD);       // отправляем наши вычисления соседям
    		    	MPI_Send(&u[k + 1][x_1 - 1], 1, MPI_DOUBLE, rank + 1, k + 1, MPI_COMM_WORLD);
    	    	}
        	}	
        	else if (rank == ROOT) {
        		if (k > 0) // главный процесс выполняет вычисления, начиная от первой точки, поэтому ему не нужны сведения о -1 точке, которая не входит в сетку
    	    		MPI_Recv(&u[k][x_1], 1, MPI_DOUBLE, rank + 1, k, MPI_COMM_WORLD, MPI_STATUS_IGNORE);      

    	    	fill_layer(k, x_1 - 1, u);

    	    	if (k < K - 2)
    		    	MPI_Send(&u[k + 1][x_1 - 1], 1, MPI_DOUBLE, rank + 1, k + 1, MPI_COMM_WORLD);
        	}
        	else { // последнему процессу не нужны сведения о M+1 точке, которая не входит в сетку, так как для точки M вычисленияя выполняются уголком
        		if (k > 0)
    	    		MPI_Recv(&u[k][x_0 - 1], 1, MPI_DOUBLE, rank - 1, k, MPI_COMM_WORLD, MPI_STATUS_IGNORE);   

    	    	fill_layer(k, x_0, u);

    	    	if(k < K - 2)
    		    	MPI_Send(&u[k + 1][x_0], 1, MPI_DOUBLE, rank - 1, k + 1, MPI_COMM_WORLD);
        	}
        	MPI_Barrier(MPI_COMM_WORLD); // следующий узел можно начинать считать только при вычислении предыдущего
        }

        if (rank == ROOT) {  // принимаем окончательные данные главным процессом
        	for (int r = 1; r < size; r++) {
                for (int k = 1; k < K; k++)
                {
                    int first = part * r + shift;
                    int last = first + part;
                    MPI_Recv(&u[k][first], part, MPI_DOUBLE, r, K, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                }
            }
        }

        else { // остальные процессы отправляют вычисленные данные
        	for (int k = 1; k < K; k++) {
        		 MPI_Send(&u[k][x_0], num_knots, MPI_DOUBLE, ROOT, K, MPI_COMM_WORLD);    
            }
        }

        if (rank == ROOT) {
            output_results(size, t1);
            if (WRITE_OUTPUT_CSV) {
                printf("[log] writing to file\n");
            // записываем решение в файл
                FILE *fp = fopen("output.csv", "w");
                fprintf(fp, "x,t,u\n"); // записываем заголовок
                for (int k = 0; k < K; k++) {
                    double t = k * t_step;
                    for (int m = 0; m < M; m++) {
                        double x = m * x_step;
                        fprintf(fp, "%.6f,%.6f,%.6f\n", x, t, u[k][m]);
                    }
                }
                fclose(fp);
            }
        }

    } else {       //выполняется в случае последовательной программы
        for (int k = 0; k < K - 1; k++) {
            for (int m = 1; m < M; m++) {
                fill_layer(k, m, u);       //последовательно заполняем слои
            }
        }
        output_results(size, t1);
    }

    for (int i = 0; i < K; i++) {
        free(u[i]);
    }
    free(u);

    MPI_Finalize();
    
    return 0;
}