import pandas as pd
import matplotlib.pyplot as plt

with open(filename, 'w', newline='') as csvfile:
    fieldnames = ['n', 'time']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

data = pd.read_csv('results.csv')

n = data['n']
time = data['time'] 

plt.plot(n, time)
plt.xlabel('n')
plt.ylabel('S')
plt.title('Ускорение S(n)')
plt.show()