import os
import csv

os.system("rm build/task.out")
os.system("rm results.csv")

filename = 'results.csv'
if not os.path.exists(filename):
    open(filename, 'w').close()

with open(filename, 'w', newline='') as csvfile:
    fieldnames = ['n', 'time']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

# os.system("mpicc -O3  main.c -o build/task.out")
os.system("mpicc main.c -o build/task.out")
for param in range(1, 15):
    # Run the MPI program with the current parameter value
    os.system(f"mpirun -np {param} --oversubscribe build/task.out")