#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <mpi.h>

#define ROOT 0
const long STEP_AMOUNT = 1000000000;
const double STEP_LENGTH = 1.0 / STEP_AMOUNT;

int main (int argc, char* argv[])
{
    double pi, sum = 0.0;

    MPI_Init (&argc, &argv);
    int rank, size;
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);
    MPI_Comm_size (MPI_COMM_WORLD, &size);

    double t1 = MPI_Wtime();
    double result = 0.0;
    double x;

    // каждый процесс считает свою часть ряда Лейбница
    for (int i = rank * STEP_AMOUNT / size; i < rank * STEP_AMOUNT / size + STEP_AMOUNT / size; i ++) {
        x = (i + 0.5) * STEP_LENGTH;
        result += 1.0 / (1.0 + x * x);
    }

    // суммируем все результаты
    MPI_Reduce(&result, &sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    double t2 = MPI_Wtime();

    // подсчет 
    if (rank == ROOT) {
        pi = STEP_LENGTH * sum * 4; // 4 из формулы Лейбница
        printf("PI = %.16lf\n", pi);
        printf("time: %lf\n", t2 - t1);
    }

    MPI_Finalize();

    return 0;
}