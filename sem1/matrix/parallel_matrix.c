#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define ROOT 0
// #define N 4 // Size of square matrix

int main(int argc, char** argv) {
    long N = atoi(argv[1]); // matrix size
    
    int rank, size, i, j;
    int A[N][N], B[N][N];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // rank of current process
    MPI_Comm_size(MPI_COMM_WORLD, &size); // total amount of processes

    if (N % size != 0) {
        printf("[err] should be N = k * size\n");
    }
    // Initialize and print matrix A
    if (rank == ROOT) {
        printf("Matrix A:\n");
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                A[i][j] = i * N + j;
                // printf("%d ", A[i][j]);
            }
            // printf("\n");
        }
    }

    // Distribute matrix A to all processes
    MPI_Bcast(&A[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);

    // Transpose matrix A
    int local_B[N/size][N];
    for (i = 0; i < N/size; i++) {
        for (j = 0; j < N; j++) {
            local_B[i][j] = A[j][i + rank*N/size];
        }
    }

    // Gather transposed matrix B
    MPI_Gather(&local_B[0][0], N*N/size, MPI_INT, &B[0][0], N*N/size, MPI_INT, 0, MPI_COMM_WORLD);

    // Print transposed matrix B on root process
    int isValid = 1;
    if (rank == ROOT) {
        printf("Matrix B:\n");
        for (i = 0; i < N; i++) {
            for (j = 0; j < N; j++) {
                // printf("%d ", B[i][j]);
                if (B[i][j] != A[j][i]) {
                    isValid = 0;
                }
            }
            // printf("\n");
        }

        if (isValid == 0) {
            printf("[err] incorrect transpose\n");
        } else {
            printf("[log] Correct transpose\n");
        }
    }

    MPI_Finalize();


    return 0;
}
