#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#define START_DATA 100
#define MAX_DATA 10000000

int main(int argc, char** argv) {

    int rank, size;
    char* message;
    double start_time, end_time;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if (rank == 0) {
        
        for ( int i = START_DATA ; i < MAX_DATA ; i+=100 ){
            double deltaTime = 0;
            for( int j = 0 ;  j < 100 ; ++j){
                char *message = (char*) malloc(i);
                start_time = MPI_Wtime();
                MPI_Send(message, i, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
                MPI_Recv(message, i, MPI_CHAR, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                end_time = MPI_Wtime();
                free(message);
                deltaTime += end_time - start_time;
            }
            printf("%d %lf\n", i, deltaTime);
        }

    } else if (rank == 1) {
        for ( int i = START_DATA ; i < MAX_DATA ; i+=100 ){
            for( int j = 0 ;  j < 100 ; ++j){
                char *message = (char*) malloc(i);
                MPI_Recv(message, i, MPI_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Send(message, i, MPI_CHAR, 0, 0, MPI_COMM_WORLD);
                free(message);
            }
        }
    }
    
    MPI_Finalize();
    return 0;
}