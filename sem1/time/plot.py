import matplotlib.pyplot as plt

with open("output.txt") as f:
  data = f.read()

data = data.split("\n")

X = []
Y = []

for i in data:
  X.append( float(i.split(" ")[0]) )
  Y.append( float(i.split(" ")[1]) )



plt.plot(X,Y)
plt.xlabel('Message size')
plt.ylabel('Average time')
plt.title('The dependence of the switching time between two nodes on the size of the message')
plt.show()