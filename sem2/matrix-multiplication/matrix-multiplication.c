#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MATRIX_SIZE 1000
// #define OPENMP_ENABLED

void fillMatrix(int** matrix, int size) {
    srand(time(NULL));
    
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            matrix[i][j] = rand() % 100;
        }
    }
}

void multiplyMatrices(int** matrix1, int** matrix2, int** result, int size) {
    #ifdef OPENMP_ENABLED
    #pragma omp parallel for
    #endif
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[i][j] = 0;
            
            for (int k = 0; k < size; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
}

void printMatrix(int** matrix, int size) {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
}

int main() {
    int** matrix1 = (int**)calloc(MATRIX_SIZE, sizeof(int*));
    int** matrix2 = (int**)calloc(MATRIX_SIZE, sizeof(int*));
    int** result = (int**)calloc(MATRIX_SIZE, sizeof(int*));
    
    for (int i = 0; i < MATRIX_SIZE; i++) {
        matrix1[i] = (int*)calloc(MATRIX_SIZE, sizeof(int));
        matrix2[i] = (int*)calloc(MATRIX_SIZE, sizeof(int));
        result[i] = (int*)calloc(MATRIX_SIZE, sizeof(int));
    }
    
    fillMatrix(matrix1, MATRIX_SIZE);
    fillMatrix(matrix2, MATRIX_SIZE);
    
    // printf("Matrix 1:\n");
    // printMatrix(matrix1, MATRIX_SIZE);
    
    // printf("\nMatrix 2:\n");
    // printMatrix(matrix2, MATRIX_SIZE);
    
    double start_time = omp_get_wtime();
    multiplyMatrices(matrix1, matrix2, result, MATRIX_SIZE);
    printf("time: %.4g seconds\n", omp_get_wtime() - start_time);

    // printf("\nResult:\n");
    // printMatrix(result, MATRIX_SIZE);
    
    for (int i = 0; i < MATRIX_SIZE; i++) {
        free(matrix1[i]);
        free(matrix2[i]);
        free(result[i]);
    }
    free(matrix1);
    free(matrix2);
    free(result);
    
    return 0;
}
