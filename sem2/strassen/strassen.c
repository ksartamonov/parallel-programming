#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define MATRIX_SIZE 128
#define OPENMP_ENABLED

void multiplyMatrices(int** matrix1, int** matrix2, int** result, int size) {
    #ifdef OPENMP_ENABLED
    #pragma omp parallel for
    #endif
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[i][j] = 0;
            
            for (int k = 0; k < size; k++) {
                result[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
}

int** createMatrix(int size) {
    int** matrix = (int**)calloc(size, sizeof(int*));
    
    for (int i = 0; i < size; i++) {
        matrix[i] = (int*)calloc(size, sizeof(int));
    }
    
    return matrix;
}

void fillMatrix(int** matrix, int size) {
    srand(time(NULL));
    
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            matrix[i][j] = rand() % 100;
        }
    }
}

void freeMatrix(int** matrix, int size) {
    for (int i = 0; i < size; i++) {
        free(matrix[i]);
    }
    
    free(matrix);
}

int** addMatrices(int** matrix1, int** matrix2, int size) {
    int** result = (int**)calloc(size, sizeof(int*));
    for (int i = 0; i < size; i++) {
        result[i] = (int*)calloc(size, sizeof(int));
    }

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }

    return result;
}

int** subtractMatrices(int** matrix1, int** matrix2, int size) {
    int** result = (int**)calloc(size, sizeof(int*));
    for (int i = 0; i < size; i++) {
        result[i] = (int*)calloc(size, sizeof(int));
    }

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[i][j] = matrix1[i][j] - matrix2[i][j];
        }
    }

    return result;
}

int** strassenMultiply(int** matrix1, int** matrix2, int size) {
    int** result = (int**)calloc(size, sizeof(int*));
    for (int i = 0; i < size; i++) {
        result[i] = (int*)calloc(size, sizeof(int));
    }

    if (size == 1) {
        result[0][0] = matrix1[0][0] * matrix2[0][0];
    } else {
        int newSize = size / 2;

        int** A11 = (int**)calloc(newSize, sizeof(int*));
        int** A12 = (int**)calloc(newSize, sizeof(int*));
        int** A21 = (int**)calloc(newSize, sizeof(int*));
        int** A22 = (int**)calloc(newSize, sizeof(int*));
        int** B11 = (int**)calloc(newSize, sizeof(int*));
        int** B12 = (int**)calloc(newSize, sizeof(int*));
        int** B21 = (int**)calloc(newSize, sizeof(int*));
        int** B22 = (int**)calloc(newSize, sizeof(int*));

        for (int i = 0; i < newSize; i++) {
            A11[i] = (int*)calloc(newSize, sizeof(int));
            A12[i] = (int*)calloc(newSize, sizeof(int));
            A21[i] = (int*)calloc(newSize, sizeof(int));
            A22[i] = (int*)calloc(newSize, sizeof(int));
            B11[i] = (int*)calloc(newSize, sizeof(int));
            B12[i] = (int*)calloc(newSize, sizeof(int));
            B21[i] = (int*)calloc(newSize, sizeof(int));
            B22[i] = (int*)calloc(newSize, sizeof(int));
        }

        #ifdef OPENMP_ENABLED
        #pragma omp parallel for
        #endif
        for (int i = 0; i < newSize; i++) {
            for (int j = 0; j < newSize; j++) {
                A11[i][j] = matrix1[i][j];
                A12[i][j] = matrix1[i][j + newSize];
                A21[i][j] = matrix1[i + newSize][j];
                A22[i][j] = matrix1[i + newSize][j + newSize];

                B11[i][j] = matrix2[i][j];
                B12[i][j] = matrix2[i][j + newSize];
                B21[i][j] = matrix2[i + newSize][j];
                B22[i][j] = matrix2[i + newSize][j + newSize];
            }
        }

        int** P1 = strassenMultiply(A11, subtractMatrices(B12, B22, newSize), newSize);
        int** P2 = strassenMultiply(addMatrices(A11, A12, newSize), B22, newSize);
        int** P3 = strassenMultiply(addMatrices(A21, A22, newSize), B11, newSize);
        int** P4 = strassenMultiply(A22, subtractMatrices(B21, B11, newSize), newSize);
        int** P5 = strassenMultiply(addMatrices(A11, A22, newSize), addMatrices(B11, B22, newSize), newSize);
        int** P6 = strassenMultiply(subtractMatrices(A12, A22, newSize), addMatrices(B21, B22, newSize), newSize);
        int** P7 = strassenMultiply(subtractMatrices(A11, A21, newSize), addMatrices(B11, B12, newSize), newSize);

        int** C11 = subtractMatrices(addMatrices(addMatrices(P5, P4, newSize), P6, newSize), P2, newSize);
        int** C12 = addMatrices(P1, P2, newSize);
        int** C21 = addMatrices(P3, P4, newSize);
        int** C22 = subtractMatrices(subtractMatrices(addMatrices(P5, P1, newSize), P3, newSize), P7, newSize);

        #ifdef OPENMP_ENABLED
        #pragma omp parallel for
        #endif
        for (int i = 0; i < newSize; i++) {
            for (int j = 0; j < newSize; j++) {
                result[i][j] = C11[i][j];
                result[i][j + newSize] = C12[i][j];
                result[i + newSize][j] = C21[i][j];
                result[i + newSize][j + newSize] = C22[i][j];
            }
        }

        for (int i = 0; i < newSize; i++) {
            free(A11[i]);
            free(A12[i]);
            free(A21[i]);
            free(A22[i]);
            free(B11[i]);
            free(B12[i]);
            free(B21[i]);
            free(B22[i]);
            free(P1[i]);
            free(P2[i]);
            free(P3[i]);
            free(P4[i]);
            free(P5[i]);
            free(P6[i]);
            free(P7[i]);
            free(C11[i]);
            free(C12[i]);
            free(C21[i]);
            free(C22[i]);
        }

        free(A11);
        free(A12);
        free(A21);
        free(A22);
        free(B11);
        free(B12);
        free(B21);
        free(B22);
        free(P1);
        free(P2);
        free(P3);
        free(P4);
        free(P5);
        free(P6);
        free(P7);
        free(C11);
        free(C12);
        free(C21);
        free(C22);
    }

    return result;
}

void printMatrix(int** matrix, int size) {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void testStrassen() {
    int size = 128;
    int** matrix1 = createMatrix(size);
    int** matrix2 = createMatrix(size);
    int** result1 = createMatrix(size);
    int** result2 = createMatrix(size);
    
    fillMatrix(matrix1, size);
    fillMatrix(matrix2, size);
    // printMatrix(matrix1, 3);
    // printMatrix(matrix2, 3);

    multiplyMatrices(matrix1, matrix2, result1, size);
    
    int** strassenResult = strassenMultiply(matrix1, matrix2, size);
    
    // printf("Traditional:\n");
    // printMatrix(result1, 3);
    // printf("Strassen:\n");
    // printMatrix(strassenResult, 3);
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            if (result1[i][j] != strassenResult[i][j]) {
                printf("Error: Strassen multiplication failed!\n");
                return;
            }
        }
    }
    
    printf("Strassen multiplication test passed!\n");
    
    freeMatrix(matrix1, size);
    freeMatrix(matrix2, size);
    freeMatrix(result1, size);
    freeMatrix(result2, size);
}

int main() {
    // int** matrix1 = createMatrix(MATRIX_SIZE);
    // int** matrix2 = createMatrix(MATRIX_SIZE);
    
    // fillMatrix(matrix1, MATRIX_SIZE);
    // fillMatrix(matrix2, MATRIX_SIZE);
    
    // printf("Matrix 1:\n");
    // printMatrix(matrix1, MATRIX_SIZE);
    
    // printf("Matrix 2:\n");
    // printMatrix(matrix2, MATRIX_SIZE);
    
    // double start_time = omp_get_wtime();
    // int** result = strassenMultiply(matrix1, matrix2, MATRIX_SIZE);
    // printf("time: %.4g seconds\n", omp_get_wtime() - start_time);

    // printf("Result:\n");
    // printMatrix(result, MATRIX_SIZE);
    
    // freeMatrix(matrix1, MATRIX_SIZE);
    // freeMatrix(matrix2, MATRIX_SIZE);
    // freeMatrix(result, MATRIX_SIZE);
    
    testStrassen();
    return 0;
}

