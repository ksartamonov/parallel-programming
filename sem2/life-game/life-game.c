#include "png-writer.h"
#include "life-game.h"

#define ITERATIONS 100

void run_scalar(size_t size)
{
    uint8_t *states = (uint8_t *)malloc(size * size * sizeof(uint8_t));
    uint32_t *counts = (uint32_t *)malloc((size + 2) * (size + 2) * sizeof(uint32_t));
    printf("[INFO] image size: %zu x %zu \n", size, size);

    randomize_states_scalar(states, size, size);

    bitmap_t image;
    image.width = size;
    image.height = size;
    image.pixels = calloc(image.width * image.height, sizeof(pixel_t));

    char imageName[255];
    size_t i = 0;
    size_t iterations = ITERATIONS;
    clock_t startTime = clock();
    for (i = 0; i < iterations; i++)
    {
        // printf("[DEBUG] iteration #%zu\n", i);
        game_of_life(counts, states, size, size);
        if (i % 33 == 0)
        {
            mapdatatoimage(&image, states);
            sprintf(imageName, "state-%zu.png", i);
            printf("[INFO] saving to %s\n", imageName);
            save_png_to_file(&image, imageName);
        }
    }

    clock_t endTime = clock();
    printf("[INFO] scalar calculation time: %lu ms \n", endTime - startTime);

    free(image.pixels);
    free(states);
    free(counts);
}

void run_vector(size_t size)
{
    size_t block = sizeof(__m256i);

    if ((size % block) != 0)
    {
        size += block - (size % block);
    }
    printf("[INFO] image size: %zu x %zu \n", size, size);
    uint8_t *states = (uint8_t *)malloc((size + 2) * (size + 2) * sizeof(uint8_t));
    uint8_t *counts = (uint8_t *)malloc((size + 2) * (size + 2) * sizeof(uint8_t));

    randomize_states_vector(states, size, size);

    bitmap_t image;
    image.width = size;
    image.height = size;
    image.pixels = calloc(image.width * image.height, sizeof(pixel_t));

    char imageName[255];
    size_t i = 0;
    size_t iterations = ITERATIONS;
    clock_t startTime = clock();

    for (i = 0; i < iterations; i++)
    {
        // printf("[DEBUG] Iteration #%zu\n", i);
        game_of_life_Vector(counts, states, size, size);
        if (i % 33 == 0)
        {
            mapdatatoimage_margin1(&image, states);
            sprintf(imageName, "state-%zu.png", i);
            printf("saving to %s\n", imageName);
            save_png_to_file(&image, imageName);
        }
        // mapdatatoimage_margin1(&image, states);
        // sprintf(imageName, "state-%zu.png", i);
        // printf("[INFO] saving %s\n", imageName);
        // save_png_to_file(&image, imageName);
    }

    clock_t endTime = clock();
    printf("[INFO] Vector calculation time: %lu ms \n", endTime - startTime);

    free(image.pixels);
    free(states);
    free(counts);
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("[ERROR] image size parameter is not present\n");
        return EXIT_FAILURE;
    }
    size_t size = atoi(argv[1]);

    if (argc > 2 && strcmp(argv[2], "-vector") == 0)
    {
        printf("[INFO] calculating in vector mode.\n");
        run_vector(size);
    }
    else
    {
        printf("[INFO] calculating in scalar mode.\n");
        run_scalar(size);
    }
    return 0;
}
