#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <x86intrin.h>

void randomize_states_scalar(uint8_t *states, size_t width, size_t height)
{
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      states[i * width + j] = rand() & 1;
    }
  }
}

void compute_neighbours_counts_scalar(uint32_t *counts, const uint8_t *states, size_t width, size_t height)
{
  memset(counts, 0, (width + 2) * (height + 2) * sizeof(uint32_t));
  size_t awidth = width + 2;
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      uint32_t val = states[i * width + j];
      counts[(i + 1 + 1) * awidth + j + 1] += val;
      counts[(i - 1 + 1) * awidth + j + 1] += val;
      counts[(i + 1) * awidth + j + 1 + 1] += val;
      counts[(i + 1) * awidth + j - 1 + 1] += val;
      counts[(i + 1 + 1) * awidth + (j + 1) + 1] += val;
      counts[(i - 1 + 1) * awidth + (j - 1) + 1] += val;
      counts[(i - 1 + 1) * awidth + j + 1 + 1] += val;
      counts[(i + 1 + 1) * awidth + (j - 1) + 1] += val;
    }
  }
}

void game_of_life(uint32_t *counts, uint8_t *states, size_t width, size_t height)
{
  compute_neighbours_counts_scalar(counts, states, width, height);
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      size_t coord = i * width + j;
      bool isAlive = states[coord];
      uint32_t neighborsAmount = counts[(i + 1) * (width + 2) + j + 1];

      if (isAlive == 1)
      {
        if (neighborsAmount < 2 || neighborsAmount > 3)
        {
          states[coord] = 0;
        }
      }
      else
      {
        if (neighborsAmount == 3)
        {
          states[coord] = 1;
        }
      }
    }
  }
}

//----------------------------------------------------------------------------------------
// http://portal.nacad.ufrj.br/online/intel/compiler_c/common/core/index.htm#GUID-D4735A59-E9C9-4614-8027-B9BDF8E15266.htm

void randomize_states_vector(uint8_t *states, size_t width, size_t height)
{
  memset(states, 0, (width + 2) * (height + 2) * sizeof(uint8_t));
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j++)
    {
      states[(i + 1) * (width + 2) + (j + 1)] = rand() & 1;
    }
  }
}

void compute_neighbours_counts_vector(uint8_t *counts, const uint8_t *states, size_t width, size_t height)
{
  size_t awidth = width + 2;
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j += sizeof(__m256i))
    {
      __m256i c =
          _mm256_setzero_si256();
      c = _mm256_add_epi8(
          c, _mm256_lddqu_si256(
                 (const __m256i *)(states + (i + 1 + 1) * awidth + j + 1)));
      c = _mm256_add_epi8(
          c, _mm256_lddqu_si256(
                 (const __m256i *)(states + (i - 1 + 1) * awidth + j + 1)));
      c = _mm256_add_epi8(
          c, _mm256_lddqu_si256(
                 (const __m256i *)(states + (i + 1) * awidth + j + 1 + 1)));
      c = _mm256_add_epi8(
          c, _mm256_lddqu_si256(
                 (const __m256i *)(states + (i + 1) * awidth + j - 1 + 1)));
      c = _mm256_add_epi8(
          c,
          _mm256_lddqu_si256(
              (const __m256i *)(states + (i + 1 + 1) * awidth + (j + 1) + 1)));
      c = _mm256_add_epi8(
          c,
          _mm256_lddqu_si256(
              (const __m256i *)(states + (i - 1 + 1) * awidth + (j - 1) + 1)));
      c = _mm256_add_epi8(
          c, _mm256_lddqu_si256(
                 (const __m256i *)(states + (i - 1 + 1) * awidth + j + 1 + 1)));
      c = _mm256_add_epi8(
          c,
          _mm256_lddqu_si256(
              (const __m256i *)(states + (i + 1 + 1) * awidth + (j - 1) + 1)));
      _mm256_storeu_si256((__m256i *)(counts + (i + 1) * awidth + (j + 1)), c);
    }
  }
}

void game_of_life_Vector(uint8_t *counts, uint8_t *states, size_t width, size_t height)
{
  assert(width % (sizeof(__m256i)) == 0);
  size_t awidth = width + 2;
  compute_neighbours_counts_vector(counts, states, width, height);
  __m256i shuffle_mask =
      _mm256_set_epi8(0, 0, 0, 0, 0, 1, 1, 0,
                      0, 0, 0, 0, 0, 1, 0, 0,
                      0, 0, 0, 0, 0, 1, 1, 0,
                      0, 0, 0, 0, 0, 1, 0, 0);
  for (size_t i = 0; i < height; i++)
  {
    for (size_t j = 0; j < width; j += sizeof(__m256i))
    {
      __m256i c = _mm256_lddqu_si256(
          (const __m256i *)(counts + (i + 1) * awidth + j + 1));
      c = _mm256_subs_epu8(
          c, _mm256_set1_epi8(
                 1)); // ограничивает максимум 7

      __m256i oldstate = _mm256_lddqu_si256(
          (const __m256i *)(states + (i + 1) * awidth + j + 1));
      __m256i newstate = _mm256_shuffle_epi8(
          shuffle_mask, _mm256_or_si256(c, _mm256_slli_epi16(oldstate, 3)));
      _mm256_storeu_si256((__m256i *)(states + (i + 1) * awidth + (j + 1)),
                          newstate);
    }
  }
}