#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <immintrin.h>

#define PRECISION 1e-6

int multiplyMatrices_double(const double* matrixA, const double* matrixB, double* matrixC, int N) {
	clock_t startTime, finishTime;
	startTime = clock();

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			for (int k = 0; k < N; k++) {
				matrixC[i * N + j] += matrixA[i * N + k] * matrixB[k * N + j];
			}
		}
	}

	finishTime = clock();
	return finishTime - startTime;
}

int multiply_matrices_SSE1(const double* matrixA, const double* matrixB, double* matrixC, int N)
{
    clock_t startTime, finishTime;
    startTime = clock();

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j += 2) {
            __m128d sum = _mm_load_pd(matrixC + i * N + j);
            for (int k = 0; k < N; k++) {
                __m128d a = _mm_set1_pd(matrixA[i * N + k]);
                __m128d b = _mm_load_pd(matrixB + k * N + j);
                sum = _mm_add_pd(_mm_mul_pd(a, b), sum);
            }
            _mm_store_pd(matrixC + i * N + j, sum);
        }
    }

    finishTime = clock();
    return finishTime - startTime;
}


int multiply_matrices_SSE2 (const double* matrixA, const double* matrixB, double* matrixC, int N)
{
	clock_t startTime, finishTime;
	startTime = clock();

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j += 2) {
			__m128d sum = _mm_load_pd(matrixC + i * N + j);
			for (int k = 0; k < N; k++) {
				sum = _mm_add_pd(_mm_mul_pd(_mm_set_pd1(matrixA[i * N + k]), _mm_load_pd(matrixB + k * N + j)), sum);
			}
			_mm_store_pd(matrixC + i * N + j, sum);
		}
	}

	finishTime = clock();
	return finishTime - startTime;
}

int multiply_matrices_AVX(const double* matrixA, const double* matrixB, double* matrixC, int N)
{
    clock_t startTime, finishTime;
    startTime = clock();

    const int block_width = N >= 256 ? 512 : 256;
    const int block_height = N >= 512 ? 8 : N >= 256 ? 16 : 32;

    for (int row_offset = 0; row_offset < N; row_offset += block_height) {
        for (int column_offset = 0; column_offset < N; column_offset += block_width) {
            for (int i = 0; i < N; ++i) {
                for (int j = column_offset; j < column_offset + block_width && j < N; j += 4) {
                    __m256d sum = _mm256_load_pd(matrixC + i * N + j);
                    for (int k = row_offset; k < row_offset + block_height && k < N; ++k) {
                        __m256d a = _mm256_set1_pd(matrixA[i * N + k]);
                        __m256d b = _mm256_load_pd(matrixB + k * N + j);
                        sum = _mm256_fmadd_pd(a, b, sum);
                    }
                    _mm256_store_pd(matrixC + i * N + j, sum);
                }
            }
        }
    }

    finishTime = clock();
    return finishTime - startTime;
}

void initializeMatrix_double(double* matrix, int N) {
    for (int i = 0; i < N; i++) {
        matrix[i] = ((int)rand() / RAND_MAX) * 100.0;
    }
}


int matricesAreEqual_double(const double* matrixA, const double* matrixB, int size) {
    for (int i = 0; i < size; i++) {
        if (fabs(matrixA[i] - matrixB[i]) > PRECISION) {
            return 0; 
        }
    }
    return 1;
}


int main() {
    int N = 1000;
    double* matrixA_double = (double*)calloc(N * N, sizeof(double));
    double* matrixB_double = (double*)calloc(N * N, sizeof(double));
    double* matrixC_double_simple = (double*)calloc(N * N, sizeof(double));

    double* matrixC_double_sse1 = (double*)calloc(N * N, sizeof(double));
    double* matrixC_double_avx = (double*)calloc(N * N, sizeof(double));
    double* matrixC_double_sse2 = (double*)calloc(N * N, sizeof(double));

    initializeMatrix_double(matrixA_double, N * N);
    initializeMatrix_double(matrixB_double, N * N);

    int simpleTime_double = multiplyMatrices_double(matrixA_double, matrixB_double, matrixC_double_simple, N);
    int avxTime = multiply_matrices_AVX(matrixA_double, matrixB_double, matrixC_double_avx, N);
    int sse1Time = multiply_matrices_SSE1(matrixA_double, matrixB_double, matrixC_double_sse1, N);
    int sse2Time = multiply_matrices_SSE2(matrixA_double, matrixB_double, matrixC_double_sse2, N);
    
    
    if (!matricesAreEqual_double(matrixC_double_avx, matrixC_double_simple, N * N)) {
        printf("[ERROR]: AVX result is not equal to simple multiplication result!\n");
    }
    if (!matricesAreEqual_double(matrixC_double_sse1, matrixC_double_simple, N * N)) {
        printf("[ERROR]: SSE1 result is not equal to simple multiplication result!\n");
    }
    if (!matricesAreEqual_double(matrixC_double_sse2, matrixC_double_simple, N * N)) {
        printf("[ERROR]: SSE2 result is not equal to simple multiplication result!\n");
    }

    printf("Simple multiplication time(double): %d ms\n", simpleTime_double);
    printf("AVX time: %d ms\n", avxTime);
    printf("SSE1 time: %d ms\n", sse1Time);
    printf("SSE2 time: %d ms\n", sse2Time);
    

    free(matrixA_double);
    free(matrixB_double);
    free(matrixC_double_simple);

    free(matrixC_double_sse1);
    free(matrixC_double_avx);
    free(matrixC_double_sse2);

    return 0;
}



