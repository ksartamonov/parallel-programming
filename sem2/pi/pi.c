#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_THREADS 4
// #define OPENMP_ENABLED

static long steps = 1000000000;
double step;

int main() {
    int num_terms = 100000000;
    double pi = 0.0;

    double start = omp_get_wtime(); 
     
    #ifdef OPENMP_ENABLED
    #pragma omp parallel for reduction(+:pi)
    #endif
    for (int i = 0; i < num_terms; i++) {
        double term = (i % 2 == 0) ? 1.0 : -1.0;
        double denominator = 2 * i + 1;
        pi += term / denominator;
    }

    pi *= 4;
    printf("Approximated value of pi: %.16g\n", pi);
    printf("time: %.4g seconds\n", omp_get_wtime() - start);
    return 0;
}
